<?php

/**
 * @file
 * Views definitions for 'cerium_docs_core_content_types'
 */
 
/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function cerium_docs_core_content_types_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'cerium_docs_core_content_types'),
    ),
    'handlers' => array(
      // The name of my handler
      'cerium_docs_core_content_types_handler_ct_icon_field' => array(
        // The name of the handler we are extending.
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
 
/**
 * Implements hook_views_data().
 */
function cerium_docs_core_content_types_views_data() {
  $data = array();
  // Add custom field
  $data['node']['ct_icon_field'] = array(
    'title' => t('Content Type Icon'),
    'help' => t('Provides an icon in the form of a span tag, based on 
    content type. Icons my be changed in the module code.'),
    'field' => array(
      'handler' => 'cerium_docs_core_content_types_handler_ct_icon_field',
    ),
  );
  return $data;
}
