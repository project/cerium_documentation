<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <div class="view-header">
    <?php
      // Add/edit/delete buttons
      $nid = $view->args[0];
      $gid = $view->args[1];

      static $topic_access_data;
      if (!isset($topic_access_data)) {
        $topic_access_data[$nid] = &drupal_static(__FUNCTION__);
      }

      if (!isset($topic_access_data['create documentation_topic content'])) {
        $topic_access_data['create documentation_topic content'] = user_access('create documentation_topic content') ? TRUE : FALSE;
      }
      print $topic_access_data['create documentation_topic content'] ? 
      '<a href="/node/add/documentation-topic?og_group_ref=' . $gid . '&field_product=' . $nid . '&destination=' . current_path()
      . '" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> Add a Topic</a> '
      : '';

      global $user;
      if (!isset($topic_access_data[$nid]['edit_node'])) {
        $topic_access_data[$nid]['edit_node'] = node_access('update', $nid, $user) ? TRUE : FALSE;
      }
      if (!isset($topic_access_data[$nid]['delete_node'])) {
        $topic_access_data[$nid]['delete_node'] = node_access('delete', $nid, $user) ? TRUE : FALSE;
      }
      
      print $topic_access_data[$nid]['edit_node'] ? ' <a href="/node/' . $nid . '/edit" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-pencil"></span> Edit Product</a>' : '';
      print $topic_access_data[$nid]['delete_node'] ? ' <a href="/node/' . $nid . '/delete" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete Product</a>' : '';
    ?>
    <?php if ($header): ?>
        <?php print $header; ?>
    <?php endif; ?>
  </div>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>

