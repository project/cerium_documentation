<?php
/**
 * @file
 * cerium_docs_update_policy.features.conditional_fields.inc
 */

/**
 * Implements hook_conditional_fields_default_fields().
 */
function cerium_docs_update_policy_conditional_fields_default_fields() {
  $items = array();

  $items["node:update_policy:0"] = array(
    'entity' => 'node',
    'bundle' => 'update_policy',
    'dependent' => 'field_duration',
    'dependee' => 'field_policy_type',
    'options' => array(
      'state' => 'visible',
      'condition' => 'value',
      'grouping' => 'AND',
      'effect' => 'show',
      'effect_options' => array(),
      'element_view' => array(
        1 => 1,
        2 => 2,
        5 => 0,
        3 => 0,
        4 => 0,
      ),
      'element_view_per_role' => 0,
      'element_view_roles' => array(
        1 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        2 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        3 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
      ),
      'element_edit' => array(
        1 => 1,
        3 => 0,
      ),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(
        1 => array(
          1 => 1,
          3 => 0,
        ),
        2 => array(
          1 => 1,
          3 => 0,
        ),
        3 => array(
          1 => 1,
          3 => 0,
        ),
      ),
      'selector' => '',
      'values_set' => 1,
      'value_form' => 'duration',
      'value' => array(
        0 => array(
          'value' => 'duration',
        ),
      ),
      'values' => array(),
    ),
  );

  $items["node:update_policy:1"] = array(
    'entity' => 'node',
    'bundle' => 'update_policy',
    'dependent' => 'field_duration',
    'dependee' => 'field_policy_type',
    'options' => array(
      'state' => 'required',
      'condition' => 'value',
      'grouping' => 'AND',
      'effect' => FALSE,
      'effect_options' => array(),
      'element_view' => array(
        1 => 1,
        2 => 2,
        5 => 0,
        3 => 0,
        4 => 0,
      ),
      'element_view_per_role' => 0,
      'element_view_roles' => array(
        1 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        2 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        3 => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
      ),
      'element_edit' => array(
        1 => 1,
        3 => 0,
      ),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(
        1 => array(
          1 => 1,
          3 => 0,
        ),
        2 => array(
          1 => 1,
          3 => 0,
        ),
        3 => array(
          1 => 1,
          3 => 0,
        ),
      ),
      'selector' => '',
      'values_set' => 1,
      'value_form' => 'duration',
      'value' => array(
        0 => array(
          'value' => 'duration',
        ),
      ),
      'values' => array(),
    ),
  );

  return $items;
}
