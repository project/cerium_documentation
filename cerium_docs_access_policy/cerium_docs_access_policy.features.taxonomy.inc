<?php
/**
 * @file
 * cerium_docs_access_policy.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function cerium_docs_access_policy_taxonomy_default_vocabularies() {
  return array(
    'access_tags' => array(
      'name' => 'Access Tags',
      'machine_name' => 'access_tags',
      'description' => 'These are special admin groups that allow a user access to multiple product groups',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
