<?php
/**
 * @file
 * cerium_docs_access_policy.features.og_features_role.inc
 */

/**
 * Implements hook_og_features_default_roles().
 */
function cerium_docs_access_policy_og_features_default_roles() {
  $roles = array();

  // Exported OG Role: 'node:product_group:May Create and Edit Documentation'.
  $roles['node:product_group:May Create and Edit Documentation'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'product_group',
    'name' => 'May Create and Edit Documentation',
  );

  return $roles;
}
