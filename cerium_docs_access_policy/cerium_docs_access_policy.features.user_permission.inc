<?php
/**
 * @file
 * cerium_docs_access_policy.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cerium_docs_access_policy_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_access_tags'.
  $permissions['create field_access_tags'] = array(
    'name' => 'create field_access_tags',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_access_tags'.
  $permissions['edit field_access_tags'] = array(
    'name' => 'edit field_access_tags',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_access_tags'.
  $permissions['edit own field_access_tags'] = array(
    'name' => 'edit own field_access_tags',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_access_tags'.
  $permissions['view field_access_tags'] = array(
    'name' => 'view field_access_tags',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_access_tags'.
  $permissions['view own field_access_tags'] = array(
    'name' => 'view own field_access_tags',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
